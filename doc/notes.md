# Fluid HTN

## Info 

https://github.com/ptrefall/fluid-hierarchical-task-network-ext
https://libraries.io/nuget/Fluid-HTN


## AIDomainDefinition

Defines the domain of the agent, that is what the agent can do.

A domain is a graph that defins the behavior of an agent.

Node typse are Select, Sequence, actions, conditions

### AIDomainBuilder

Builds AIDomainDefinitions, aka Actions.


## AIWorldState

AIWorldState is an enum defining name of states, its types of entries in FluidHTNs blackbord which is called AIContext . This should be replaced with a Blackboard.

It works together with AIContext which is FluidHTNs version of blackboard.

## AIContext

AIContext is a blackboard handling all the knowlege of the agent.


## Bag

Bag is an inventory of an agent.

## Senses

Senses is a set of sensors.

## Planner

The Planner do the real job.

## AIAgent

AIAgent is the agent that owns the AI. 

## Tasks

Primitive Task and Compound Task.


### Primitive Tasks

Primitive tasks represent a single step that can be performed by the agent. Primitive tasks are comprised of an operator and sets of effects and conditions.

The operator represents an atomic action that an agent can do.

### Compound Tasks

There are two types of Compound Tasks, Selectors and Sequencers. A Selector selects one of its subtasks to decompose. A Sequence decompose all of its subtasks.

## Conditions

Conditions are boolean validators that can be used to validate the decomposition of a compound task, or the validity of a primitive task. Primitive Tasks also have Executing Conditions, which we validate before every update to the primary task's operator during execution of a plan.

### Executing Conditions

Advanced: Executing Conditions are special conditions that are evaluted every planner tick. These special conditions are useful for cases where you need to re-evaluate the validity of your conditions after planning and after setting a new task as current task during execution (e.g. the operator of the task will return continue). In this case where the operator returns continue and the condition becomes "invalid", the planner will not automatically know. Executing Conditions is one way to ensure that the planner realize that the task is now invalid. Another way is to put this logic inside the Operator and have it return Failure, which should yield the same result in practice (it triggers a replan).

## Operators

Operators are the logic operation a primitive task should perform during plan execution. Every time an operator updates, it returns a status whether it succeeded, failed or need to continue next tick.


## Effects

Effects apply world state change during planning, and optionally during execution. They can only be applied to primitive tasks. There are three types of effects.

    PlanOnly effects temporarily change the world state during planning, used as a prediction about the future. Its change on the world state is removed before plan execution. This can be useful when we need other systems to set the world state during execution.
    PlanAndExecute effects work just like PlanOnly effects, only that during execution, when the task they represent complete its execution successfully, the effect is re-applied. This is useful in the cases where you don't have other systems to set the world state during execution.
    Permanent effects are applied during planning, but not removed from the world state before execution. This can be very useful when there's some state we change only during planning, e.g. do this thing three times then do this other thing. It can also be useful when we want to update world state in our runtime code, where we definitely want our changes to be permanent.
