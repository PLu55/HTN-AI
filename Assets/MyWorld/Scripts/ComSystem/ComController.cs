using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
 
namespace PLu.ComSystem
{
    public class ComController
    {
    [System.Serializable]
    public class SimpleEvent : UnityEvent<string> {}

    [System.Serializable]
    public class StringEvent : UnityEvent<string, string> {}

    [System.Serializable]
    public class FloatEvent : UnityEvent<string, float> {}

    [System.Serializable]
    public class IntEvent : UnityEvent<string, int> {}

    [System.Serializable]
    public class BoolEvent : UnityEvent<string, bool> {}

    [System.Serializable]
    public class PositionEvent : UnityEvent<string, Vector3> {}

    public static SimpleEvent OnSimpleEvent = new SimpleEvent();
    public static PositionEvent OnPositionEvent = new PositionEvent(); 
    public static StringEvent OnStringEvent = new StringEvent();
    public static FloatEvent OnFloatEvent = new FloatEvent();
    public static IntEvent OnIntEvent = new IntEvent();
    public static BoolEvent OnBoolEvent = new BoolEvent();

        private static ComController _instance;

        public static ComController Instance
        {
            get
            {

                if (_instance == null)
                {
                    _instance = new ComController();
                }
                return _instance;
            }
        }

        public void SimpleMessage(string message)
        {
            OnSimpleEvent?.Invoke(message);
        }
        public void PositionMessage(string message, Vector3 position)
        {
            OnPositionEvent?.Invoke(message, position);
        }
        public void StringMessage(string message, string value)
        {
            OnStringEvent?.Invoke(message, value);
        }
        public void FloatMessage(string message, float value)
        {
            OnFloatEvent?.Invoke(message, value);
        }
        public void IntMessage(string message, int value)
        {
            OnIntEvent?.Invoke(message, value);
        }
        public void BoolMessage(string message, bool value)
        {
            OnBoolEvent?.Invoke(message, value);
        }
    }
}
