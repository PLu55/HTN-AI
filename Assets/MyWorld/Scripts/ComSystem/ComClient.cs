using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.ComSystem
{
    public class ComClient : MonoBehaviour
    {
        private ComController _comController;
        void Start()
        {
            //ComController.OnBroadcast += HandleBroadcast;
        }

        public void Subscribe()
        {

        }
        public void UnSubscribe()
        {
            
        }
        void HandleBroadcast(string message)
        {
            // Do something with the message
        }

        void OnDestroy()
        {
            // Always unsubscribe from events when the object is destroyed to prevent memory leaks
            //ComController.OnBroadcast -= HandleBroadcast;
        }
        public void OnRecieve(string message)
        {
            Debug.Log($"Recieving message:  {message}");
        }
        public void Broadcast(string message)
        {
            Debug.Log($"Sending message:  {message}");
        }
    }


    public class Receiver : MonoBehaviour
    {
        private void OnEnable()
        {
            ComController.OnPositionEvent.AddListener(HandlePositionEvent);
            ComController.OnStringEvent.AddListener(HandleStringEvent);
            ComController.OnFloatEvent.AddListener(HandleFloatEvent);
            ComController.OnIntEvent.AddListener(HandleIntEvent);
            ComController.OnBoolEvent.AddListener(HandleBoolEvent);
        }

        private void OnDisable()
        {
            ComController.OnPositionEvent.RemoveListener(HandlePositionEvent);
            ComController.OnStringEvent.RemoveListener(HandleStringEvent);
            ComController.OnFloatEvent.RemoveListener(HandleFloatEvent);
            ComController.OnIntEvent.RemoveListener(HandleIntEvent);
            ComController.OnBoolEvent.RemoveListener(HandleBoolEvent);
        }

        private void HandlePositionEvent(string message, Vector3 position)
        {
            // Handle the position event
        }

        private void HandleStringEvent(string message, string value)
        {
            // Handle the string event
        }

        private void HandleFloatEvent(string message, float value)
        {
            // Handle the float event
        }

        private void HandleIntEvent(string message, int value)
        {
            // Handle the int event
        }

        private void HandleBoolEvent(string message, bool value)
        {
            // Handle the bool event
        }
    }
}