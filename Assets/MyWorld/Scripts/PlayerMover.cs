using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace PLu
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] private float _speed = 5f;
        [SerializeField] private float _turnSpeed = 100f;

        public float Speed => _speed;

        void Update()
        {
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                float horizontalInput = Input.GetAxis("Horizontal");
                float verticalInput = Input.GetAxis("Vertical");
                
                float rotationAmount = horizontalInput * _turnSpeed * Time.deltaTime;
                transform.Rotate(Vector3.up, rotationAmount);

                Vector3 movement = new Vector3(0f, 0f, verticalInput * _speed * Time.deltaTime);
                transform.Translate(movement);
            }

        }
    }
}
