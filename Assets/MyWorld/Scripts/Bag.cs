using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyWorld
{
    public class Bag : MonoBehaviour
    {
        [SerializeField]
        Dictionary<AIResourceType, float> resources;

        void Awake()
        {
            resources = new Dictionary<AIResourceType, float>();
        }

        public float GetResource(AIResourceType resourceType)
        {
            if (resources.ContainsKey(resourceType))
                return resources[resourceType];
            else
                return 0f;
        }

        public void SetResource(AIResourceType resourceType, float amount)
        {
            resources[resourceType] = amount;
        }
        public void AddResource(AIResourceType resourceType, float amount)
        {
            float ownedAmount = 0f;
            if (resources.ContainsKey(resourceType))
                ownedAmount = GetResource(resourceType);
            resources[resourceType] = ownedAmount + amount;
            Debug.Log($"Bag has {resources[resourceType]} of {resourceType}.");
        }
    }
}