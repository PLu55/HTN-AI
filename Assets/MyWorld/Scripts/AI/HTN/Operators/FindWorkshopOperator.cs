
using UnityEngine;
using FluidHTN;
using FluidHTN.Operators;

namespace MyWorld
{
    public class FindWorkshopOperator : IOperator
    {
    public WorkshopType WorkshopType { get; }
        public FindWorkshopOperator(WorkshopType workshopType)
        {
            WorkshopType = workshopType;
        }        
        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                c.CurrentWorkshop = null;
                if (c.CurrentWorkshop == null)
                {
                    float closestSqrDist = float.MaxValue;
                    Workshop closestWorkshop = null;
                    foreach (var workshop in c.KnownWorkshops)
                    {
                        if (workshop.WorkshopType == WorkshopType )
                        {
                            float sqrDist = Mathf.Abs((c.Agent.transform.position - workshop.transform.position).sqrMagnitude);
                            if ( sqrDist < closestSqrDist)
                            {
                                closestWorkshop = workshop;
                                closestSqrDist = sqrDist;
                            }
                        }
                    }
                    if (closestWorkshop == null)
                        Debug.Log($"FindWorkshopOperator.Update didn't find any {WorkshopType}");
                    if (closestWorkshop != null)
                    {
                        Debug.Log($"FindWorkshopOperator.Update {closestWorkshop}");
                        c.CurrentWorkshop = closestWorkshop;
                       return TaskStatus.Success;
                    }
                }
            }
            Debug.Log($"FindWorkshopOperator.Update <== Faliure!");
            return TaskStatus.Failure;
        }

        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
                c.CurrentWorkshop = null;
        }
    }
}