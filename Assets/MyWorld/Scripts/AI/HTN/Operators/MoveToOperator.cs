using FluidHTN;
using FluidHTN.Operators;
using UnityEngine;


namespace MyWorld
{
    public class MoveToOperator : IOperator
    {
        public AIDestinationTarget DestinationTarget { get; }

        public MoveToOperator(AIDestinationTarget destinationTarget)
        {
            DestinationTarget = destinationTarget;
        }

        public TaskStatus StartNavigation(AIContext ctx)
        {   
            if (DestinationTarget == AIDestinationTarget.Resource)
            {
                if (ctx.CurrentResource == null)
                {
                    return TaskStatus.Failure;
                }

                ctx.NavAgent.isStopped = false;
                if (ctx.NavAgent.SetDestination(ctx.CurrentResource.transform.position))
                {
                    return TaskStatus.Continue;
                }
            }
            else if (DestinationTarget == AIDestinationTarget.Workshop)
            {
                if (ctx.CurrentWorkshop == null)
                {
                    return TaskStatus.Failure;
                }

                ctx.NavAgent.isStopped = false;
                if (ctx.NavAgent.SetDestination(ctx.CurrentWorkshop.transform.position))
                {
                    return TaskStatus.Continue;
                }
            }
            else if (DestinationTarget == AIDestinationTarget.Enemy)
            {
                if (ctx.CurrentTarget == null)
                {
                    return TaskStatus.Failure;
                }

                ctx.NavAgent.isStopped = false;
                if (ctx.NavAgent.SetDestination(ctx.CurrentTarget.transform.position))
                {
                    return TaskStatus.Continue;
                }
            }
            
            return TaskStatus.Failure;
        }

        public TaskStatus UpdateNavigation(AIContext c)
        {

            if (DestinationTarget == AIDestinationTarget.Resource)
            {
                if (c.CurrentResource == null)
                {
                    return TaskStatus.Failure;
                }

                if (c.NavAgent.remainingDistance <= c.NavAgent.stoppingDistance)
                {
                    c.CurrentResource.LastTimeVisited = c.CurrentTime;
                    //c.CurrentResource = null;
                    c.NavAgent.isStopped = true;
                    return TaskStatus.Success;
                }

                return TaskStatus.Continue;
            }
            else if (DestinationTarget == AIDestinationTarget.Workshop)
            {
                if (c.CurrentWorkshop == null)
                {
                    return TaskStatus.Failure;
                }

                if (c.NavAgent.remainingDistance <= c.NavAgent.stoppingDistance)
                {
                    c.NavAgent.isStopped = true;
                    return TaskStatus.Success;
                }

                return TaskStatus.Continue;
            }
            /*
            else if (DestinationTarget == AIDestinationTarget.Enemy)
            {
                if (c.CurrentEnemy == null)
                {
                    return TaskStatus.Failure;
                }

                if (c.NavAgent.remainingDistance <= c.NavAgent.stoppingDistance)
                {
                    c.NavAgent.isStopped = true;
                    return TaskStatus.Success;
                }

                if (c.NavAgent.SetDestination(c.CurrentEnemy.transform.position))
                {
                    return TaskStatus.Continue;
                }
            }
            */

            return TaskStatus.Failure;
        }

        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                if (c.NavAgent.isStopped)
                {
                    return StartNavigation(c);
                }
                else
                {
                    return UpdateNavigation(c);
                }
            }

            return TaskStatus.Failure;
        }

        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                if (DestinationTarget == AIDestinationTarget.Resource)
                {
                    c.CurrentResource = null;
                }
                c.NavAgent.isStopped = true;
            }
        }
    }
}