using FluidHTN;
using FluidHTN.Operators;
using UnityEngine;


namespace MyWorld
{ 
    public class PatrolOperator : IOperator
    {
        private PatrolPath _patrolPath;
        public PatrolPath PatrolPath => _patrolPath;

        private int _patrolWaypointIndex = -1;
        private Vector3 _targetPosition;

        public PatrolOperator() {}

         public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                if (c.NavAgent.isStopped)
                {
                    return StartNavigation(c);
                }
                else
                {
                    return UpdateNavigation(c);
                }
            }

            return TaskStatus.Failure;
        }

        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                _patrolWaypointIndex = -1;
                _targetPosition = Vector3.zero;
                c.NavAgent.isStopped = true;
            }
        }
               private bool FindClosestWaypoint(Vector3 position)
        {
            float closestDistanceSquared = Mathf.Infinity;
            for (int i = 0; i < _patrolPath.PatrolPoints.Count; i++)
            {
                var waypoint = _patrolPath.PatrolPoints[i];
                var distanceSquared = (position - waypoint.position).sqrMagnitude;

                if (distanceSquared < closestDistanceSquared)
                {
                    _patrolWaypointIndex = i;
                    closestDistanceSquared = distanceSquared;
                }
            }
            if (_patrolWaypointIndex != -1)
            {
                _targetPosition = _patrolPath.PatrolPoints[_patrolWaypointIndex].position;
                return true;
            } 
            return false;;
        }
        private TaskStatus StartNavigation(AIContext ctx)
        {   
            _patrolPath = ctx.Agent.GetComponent<PatrolPath>();
            if (_patrolPath == null || _patrolPath.PatrolPoints.Count == 0)
            {
                return TaskStatus.Failure;
            }

            if (FindClosestWaypoint(ctx.Position))
            {
                ctx.NavAgent.isStopped = false;

                if (ctx.NavAgent.SetDestination(_targetPosition))
                {
                    return TaskStatus.Continue;
                }
            }

            ctx.NavAgent.isStopped = true;
            return TaskStatus.Failure;
        }
        private TaskStatus UpdateNavigation(AIContext ctx)
        {
            if (ctx.NavAgent.remainingDistance <= ctx.NavAgent.stoppingDistance)
            {
                _patrolWaypointIndex = (_patrolWaypointIndex + 1) % _patrolPath.PatrolPoints.Count;
                _targetPosition = _patrolPath.PatrolPoints[_patrolWaypointIndex].position;
                if (ctx.NavAgent.SetDestination(_targetPosition))
                {
                    return TaskStatus.Continue;
                }
                return TaskStatus.Failure;
            }
            return TaskStatus.Continue;
        }
    }
}