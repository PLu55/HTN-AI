using System.Collections;
using System.Collections.Generic;
using FluidHTN;
using FluidHTN.Operators;
using UnityEngine;
 
namespace MyWorld
{
    public class ChaseTargetOperator : IOperator
    {

        private Vector3 _targetPosition;
        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                if (c.HasState(AIWorldState.HasEnemyInSight, false) || 
                    c.CurrentTarget == null)
                {
                    Stop(c);
                    return TaskStatus.Failure;
                }

                if (c.NavAgent.isStopped)
                {
                    return StartNavigation(c);
                }
                else
                {
                    return UpdateNavigation(c);
                }
            }

            return TaskStatus.Failure;
        }
        
        public void Stop(IContext ctx)
        {
            if (ctx is AIContext context)
            context.NavAgent.isStopped = true;
        }

        private TaskStatus StartNavigation(AIContext ctx)
        {
            _targetPosition = ctx.CurrentTarget.transform.position;
            ctx.NavAgent.isStopped = false;

            if (ctx.NavAgent.SetDestination(_targetPosition))
            {
                    return TaskStatus.Continue;
            }

            ctx.NavAgent.isStopped = true;
            return TaskStatus.Failure;
        }
        private TaskStatus UpdateNavigation(AIContext ctx)
        {
            if (ctx.NavAgent.remainingDistance <= ctx.NavAgent.stoppingDistance)
            {
                ctx.NavAgent.isStopped = true;
                return TaskStatus.Success;
            }
            _targetPosition = ctx.CurrentTarget.transform.position;

            if (ctx.NavAgent.SetDestination(_targetPosition))
            {
                return TaskStatus.Continue;
            }

            return TaskStatus.Failure;
        }
    }
}
