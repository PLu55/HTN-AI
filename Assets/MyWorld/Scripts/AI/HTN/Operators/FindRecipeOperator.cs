using UnityEngine;
using FluidHTN;
using FluidHTN.Operators;

namespace MyWorld
{
    public class FindRecipeOperator : IOperator
    {
        public AIResourceType Artefact { get; }         
        public FindRecipeOperator(AIResourceType artefact)
        {
            Artefact = artefact;
        } 
        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                if (c.CurrentRecipe == null)
                {
                    foreach (var recipe in c.KnownRecipes)
                    {
                        if (recipe.Artefact == Artefact)
                        {
                            c.CurrentRecipe = recipe;
                            Debug.Log($"FindRecipeOperator.Update <{Artefact}><== Success!");
                            return TaskStatus.Success;
                        }
                    }
                }
            }
            Debug.Log($"FindRecipeOperator.Update <== Faliure!");
            return TaskStatus.Failure;
        }
        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
                c.CurrentRecipe = null;
        }
    }
}