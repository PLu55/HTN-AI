using System.Collections;
using System.Collections.Generic;
using FluidHTN;
using FluidHTN.Operators;
using UnityEngine;
using UnityEngine.AI;
using Utilities;
 
namespace MyWorld
{
    public class SearchTargetOperator : IOperator
    {
        private Vector3 _targetPosition;
        private CountdownTimer _searchTimer;

        public SearchTargetOperator() {}

        public TaskStatus Update(IContext context)
        {
            //Debug.Log("SearchTargetOperator Update");
            if (context is AIContext ctx)
            {
                if (false && ctx.CurrentTarget == null)
                {
                    //Debug.Log("SearchTargetOperator CurrentTarget is null");
                    return TaskStatus.Failure;
                }


                if (ctx.NavAgent.isStopped)
                {
                    return StartNavigation(ctx);
                }
                else
                {
                    return UpdateNavigation(ctx);
                }
            }

            return TaskStatus.Failure;
        }
        
        public void Stop(IContext ctx)
        {
            if (ctx is AIContext context)
            context.NavAgent.isStopped = true;
        }

        private Vector3 SwapYZ(Vector3 v)
        {
            return new Vector3(v.x, v.z, v.y);
        }

        private bool ComputePosition(ref Vector3 position, float radius, int tries = 5)
        {
            for (int i = 0; i < tries; i++)
            {
                var randomMovement = SwapYZ(Random.insideUnitSphere * radius);

                var randomPosition = position + randomMovement;

                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPosition, out hit, radius, NavMesh.AllAreas))
                {
                    position = hit.position;
                    return true;
                }
            }
            return false;
        }
        private TaskStatus StartNavigation(AIContext ctx)
        {
            //Debug.Log("SearchTargetOperator StartNavigation");
            ctx.NavAgent.isStopped = false;
            _searchTimer = new CountdownTimer(ctx.Agent.SearchDuration);
            _searchTimer.Start();
            _targetPosition = ctx.LastTargetPosition;
            return TaskStatus.Continue;
        }
        private TaskStatus UpdateNavigation(AIContext ctx)
        {
            //Debug.Log("SearchTargetOperator UpdateNavigation");
            if (_searchTimer == null)
            {
                return TaskStatus.Failure;
            }

            _searchTimer.Tick(Time.deltaTime);

            if (_searchTimer.IsFinished)
            {
                Debug.Log("SearchTargetOperator _searchTimer.IsFinished");
                ctx.NavAgent.isStopped = true;
                return TaskStatus.Success;
            }
            
            if (ctx.NavAgent.remainingDistance <= ctx.NavAgent.stoppingDistance)
            {
                
                if (ComputePosition(ref _targetPosition, ctx.Agent.SearchRadius))
                {
                    //Debug.Log("SearchTargetOperator set new destination");
                    if (ctx.NavAgent.SetDestination(_targetPosition))
                    {
                        //Debug.Log("SearchTargetOperator return TaskStatus.Continue");
                        return TaskStatus.Continue;
                    }
                }
                //Debug.Log("SearchTargetOperator return NO new position");
                ctx.NavAgent.isStopped = true;
                return TaskStatus.Continue;
            }

            return TaskStatus.Continue;
        }
    }
}
