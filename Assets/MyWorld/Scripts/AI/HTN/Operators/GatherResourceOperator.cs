
using UnityEngine;
using FluidHTN;
using FluidHTN.Operators;

namespace MyWorld
{
    public class GatherResourceOperator : IOperator
    {
        public AIResourceType ResourceType { get; }
        public float Amount { get; }

        public GatherResourceOperator(AIResourceType resourceType, float amount)
        {
            ResourceType = resourceType;
            Amount = amount;
            Debug.Log($"GatherResourceOperator.Instanciate({resourceType}, {amount})");
        }        
        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                Debug.Log($"GatherResourceOperator.Update CurrentResource is set: {c.CurrentResource != null }");
                if (c.CurrentResource?.ResourceType == ResourceType &&
                    c.CurrentResource.Amount >= Amount)
                {
                    float amount = Mathf.Min(c.CurrentResource.Amount, Amount);
                    c.CurrentResource.Amount = c.CurrentResource.Amount - amount;
                    c.Agent.Bag.AddResource(ResourceType, amount);
                    if (c.CurrentResource.Amount <= 0)
                    {
                        c.CurrentResource = null; // how to stop???
                    }
                    Debug.Log($"GatherResourceOperator.Update <= Success");
                    c.CurrentResource = null;
                    return TaskStatus.Success;
                }
            }
            return TaskStatus.Failure;
        }

        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
                c.CurrentResource = null;
        }
    }
}