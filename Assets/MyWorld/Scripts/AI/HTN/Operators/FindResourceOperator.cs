
using UnityEngine;
using FluidHTN;
using FluidHTN.Operators;

namespace MyWorld
{
    public class FindResourceOperator : IOperator
    {
        public AIResourceType ResourceType { get; }

        public FindResourceOperator(AIResourceType resourceType)
        {
            ResourceType = resourceType;
        }        
        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                c.CurrentResource = null;
                if (c.CurrentResource == null)
                {
                    float closestSqrDist = float.MaxValue;
                    Resource closestResource = null;
                    foreach (var resource in c.KnownResources)
                    {
                        if (resource.ResourceType == ResourceType && resource.Amount > 0f)
                        {
                            float sqrDist = Mathf.Abs((c.Agent.transform.position - resource.transform.position).sqrMagnitude);
                            if ( sqrDist < closestSqrDist)
                            {
                                closestResource = resource;
                                closestSqrDist = sqrDist;
                            }
                        }
                    }
                    if (closestResource == null)
                        Debug.Log($"FindResourceOperator.Update didn't find any {ResourceType}");
                    if (closestResource != null)
                    {
                        Debug.Log($"FindResourceOperator.Update {closestResource}");
                        c.CurrentResource = closestResource;
                       return TaskStatus.Success;
                    }
                }
            }
            Debug.Log($"FindResourceOperator.Update <== Faliure!");
            return TaskStatus.Failure;
        }

        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
                c.CurrentResource = null;
        }
    }
}