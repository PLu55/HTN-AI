
using UnityEngine;
using FluidHTN;
using FluidHTN.Operators;

namespace MyWorld
{
    public class MakeRecipeOperator : IOperator
    {
        public AIResourceType ResourceType { get; }
        public float Amount { get; }

        public MakeRecipeOperator(AIResourceType resourceType, float amount = 1f)
        {
            ResourceType = resourceType;
            Amount = amount;
            Debug.Log($"MakeRecipeOperator.Instanciate({resourceType}, {Amount})");
        }        
        public TaskStatus Update(IContext ctx)
        {
            if (ctx is AIContext c)
            {
                Debug.Log($"MakeRecipeOperator.Update CurrentRecpie is set: {c.CurrentRecipe != null }");
                if (c.CurrentRecipe is Recipe recipe && 
                    recipe.HasResources(c.Agent.Bag))
                {
                    recipe.CreateArtefact(c.Agent.Bag);
                    c.CurrentRecipe = null;
                    c.WorldState.SetValue(AIWorldState.HasIngredient, 0);
                    Debug.Log($"MakeRecipeOperator.Update <= Success");

                    return TaskStatus.Success;
                }
            }
            Debug.Log($"MakeRecipeOperator.Update <= Failure");
            return TaskStatus.Failure;
        }

        public void Stop(IContext ctx)
        {
            if (ctx is AIContext c)
                c.CurrentResource = null;
        }
    }
}