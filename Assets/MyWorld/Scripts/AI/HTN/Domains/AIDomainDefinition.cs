using FluidHTN;
using UnityEngine;

namespace MyWorld
{
    public abstract class AIDomainDefinition : ScriptableObject
    {
        public abstract Domain<AIContext> Create();
    }
}