
using System.Runtime.InteropServices.WindowsRuntime;
using FluidHTN;
using UnityEngine;
/*
namespace MyWorld
{
    [CreateAssetMenu(fileName = "HumanDomain", menuName = "MyAI/Domain/Human")]
    public class HumanDomainDefinition : AIDomainDefinition
    {
        public override Domain<AIContext> Create()
        {
            return new AIDomainBuilder("Human")
                .Sequence("Do Recipe")
                 .End()
                 .Buld();   
        }
    }
}
*/

namespace MyWorld
{
    [CreateAssetMenu(fileName = "GuardDomain", menuName = "MyAI/Domain/Guard")]
    public class GuardDomainDefinition : AIDomainDefinition
    {
        public override Domain<AIContext> Create() 
        {
            return new AIDomainBuilder("Be a Guard")
                .Sequence("Guard")
                    .HasState(AIWorldState.HasEnemyInSight)
                    .HasState(AIWorldState.HasEnemy, false)
                    .Action("Found Enmy")
                        .Do((ctx) => { return TaskStatus.Success; })
                            //.Effect("has enemy", EffectType.PlanAndExecute, (ctx, type) => { ctx.SetState(AIWorldState.HasEnemy, true, type); })
                        .SetState(AIWorldState.HasEnemy, true, EffectType.Permanent)
                    .End()
                .End()                    
                .Select("Guarding")
                    .Sequence("Search")
                        .HasState(AIWorldState.HasEnemyInSight, false)
                        .HasState(AIWorldState.HasEnemy)
                        .SearchTarget()
                    .End()
                    .Sequence("Patrol")
                        .HasState(AIWorldState.HasEnemyInSight, false)
                        .Wait(3f)
                        .Patrol()
                    .End()
                    .Sequence("Chase")
                        .HasState(AIWorldState.HasEnemyInSight)
                        .HasState(AIWorldState.HasEnemy)
                        .ChaseTarget()
                    .End()
                .End()
                .Build();
        }
    }
}
// namespace MyWorld
// {
//     [CreateAssetMenu(fileName = "GuardDomain", menuName = "MyAI/Domain/Guard")]
//     public class GuardDomainDefinition : AIDomainDefinition
//     {
//         public override Domain<AIContext> Create()
//         {
//             return new AIDomainBuilder("Guard")
//                 .Select("Guard")
//                     .Select("Find Enemy")
//                         .Sequence("Search")
//                             .HasState(AIWorldState.HasEnemyInSight, false)
//                             .SearchTarget()
//                         .End()
//                         .Sequence("Chase")
//                             .HasState(AIWorldState.HasEnemyInSight)
//                             .ChaseTarget()
//                         .End()
//                     .End()
//                     .Sequence("Attack")
//                         .HasState(AIWorldState.HasEnemyInSight)
//                         .ChaseTarget()
//                         .Wait(2f)
//                         //.AttackEnemy()
//                     .End()
//                      .Sequence("Patrol")
//                     //     .HasState(AIWorldState.HasPatrolPath)
//                         .HasState(AIWorldState.HasEnemyInSight, false)
//                         .Wait(3f)
//                         .Patrol()
//                     .End()

//                     // .Sequence("Investigate")
//                     //     .HasState(AIWorldState.HasHeardNoise)
//                     //     .MoveToNoise()
//                     //     .Wait(2f)
//                     //     .InvestigateNoise()
              
//                 .End()
//                 .Build();
//         }
//     }
// }