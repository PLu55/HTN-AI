
using FluidHTN;
using UnityEngine;
/*
namespace MyWorld
{
    [CreateAssetMenu(fileName = "HumanDomain", menuName = "MyAI/Domain/Human")]
    public class HumanDomainDefinition : AIDomainDefinition
    {
        public override Domain<AIContext> Create()
        {
            return new AIDomainBuilder("Human")
                .Sequence("Do Recipe")
                 .End()
                 .Buld();   
        }
    }
}
*/

namespace MyWorld
{
    [CreateAssetMenu(fileName = "HumanDomain", menuName = "MyAI/Domain/Human")]
    public class HumanDomainDefinition : AIDomainDefinition
    {
        public override Domain<AIContext> Create()
        {
            return new AIDomainBuilder("Human")
                .Select("Collect Resources")
                    .Sequence("Make Recipe")
                        .HasState(AIWorldState.HasIngredient, 3)
                        .FindWorkshop(WorkshopType.Smithy)
                        .MoveToWorkshop()
                        .Wait(2f)
                        .MakeRecipe(AIResourceType.Axe)

                    .End()  
                    .Sequence("Get Wood")
                        //.Condition("Hasn't wood", (ctx) => !ctx.HasState())
                        .HasFlagState(AIWorldState.HasIngredient, 0, false)
                        .HasState(AIWorldState.HasResourceInSight)
                        .FindResource(AIResourceType.Wood)
                        .MoveToResource()
                        .Wait(2f)
                        .GatherResource(AIResourceType.Wood, 0.5f, 0)
                    .End()
                    .Sequence("Get Iron Ore")
                        .HasFlagState(AIWorldState.HasIngredient, 1, false)
                        .HasState(AIWorldState.HasResourceInSight)
                        .FindResource(AIResourceType.IronOre)
                        .MoveToResource()
                        .Wait(2f)
                        .GatherResource(AIResourceType.IronOre, 0.5f, 1)
                    .End()
              
                .End()
                .Build();
        }
    }
}