
using FluidHTN;
using UnityEngine;

namespace MyWorld
{
    public class ResourceSensor : Sensor, ISensor
    {
        private static Collider[] _hits = new Collider[128];        

        public override void Tick(AIContext context)
        {
            context.KnownResources.Clear();
            var hitCount = Physics.OverlapSphereNonAlloc(context.Position, context.Senses.EyeSight, _hits);
            if (hitCount > 0)
            {
                for (var i = 0; i < hitCount; i++)
                {
                    var resource = _hits[i].GetComponent<Resource>();
                    if (resource != null)
                    {
                        context.KnownResources.Add(resource);
                    }
                }
            }

            context.SetState(AIWorldState.HasResourceInSight, context.KnownResources.Count > 0, EffectType.Permanent);
        }

        public override void DrawGizmos(AIContext context)
        {
            foreach (var resource in context.KnownResources)
            {
                if (resource == context.CurrentResource)
                {
                    Gizmos.color = new Color(0f, 1f, 1f, 0.85f);
                }
                else
                {
                    Gizmos.color = new Color(0f, 1f, 1f, 0.15f);
                }
                Gizmos.DrawLine(context.Head.position, resource.transform.position + Vector3.up * 2f);
                Gizmos.DrawSphere(resource.transform.position + Vector3.up * 2f, 0.25f);
            }
        }
    }
}