
using FluidHTN;
using UnityEngine;

namespace MyWorld
{
    public class TagedSightSensor : Sensor, ISensor
    {
        [SerializeField] private string _tag = "Player";
        [SerializeField] private float _sightAngle = 160f;
        [SerializeField] private bool _debug = false;
        private float _sightCos = 0.1f;

        private static Collider[] _hits = new Collider[128];

        public float NextTickTime { get; set; }

        new void Start()
        {
            if (_debug) Debug.Log("TagedSightSensor Start");
            base.Start();
            _sightCos = Mathf.Cos(_sightAngle * (Mathf.PI / 360.0f)); // 1 - Acos(phi / 2)
        }
        public override void Tick(AIContext context)
        {
            var hitCount = Physics.OverlapSphereNonAlloc(context.Position, context.Senses.EyeSight, _hits);
            bool state = false;
            context.CurrentTarget = null;
            for (var i = 0; i < hitCount; i++)
            {
                if (_hits[i].gameObject.CompareTag(_tag))
                {
                    GameObject target = _hits[i].gameObject;
                    
                    float cos = Vector3.Dot(transform.forward , (target.transform.position - transform.position).normalized);

                    if (cos < _sightCos)
                    {
                        
                        break;
                    }

                    context.CurrentTarget = target; 
                    context.LastTargetPosition = target.transform.position;
                    state = true;
                    break;
                }
            }
            if (! context.HasState(AIWorldState.HasEnemyInSight, state))
            {
                if (_debug) Debug.Log($"PlayerSensor: Change state HasEnemyInSight to: {state}");
                context.SetState(AIWorldState.HasEnemyInSight, state, EffectType.Permanent);
            }
        }
        public override void DrawGizmos(AIContext context)
        {
            if (context.CurrentTarget != null)
            {
                Vector3 position = context.CurrentTarget.transform.position;
                Gizmos.color = new Color(0f, 1f, 1f, 0.15f);
                Gizmos.DrawLine(context.Agent.transform.position, position + Vector3.up * 2f);
                Gizmos.DrawSphere(position + Vector3.up * 2f, 0.25f);
            }

        }
    }
}