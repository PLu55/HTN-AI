using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using MyWorld;


namespace MyWorld

{
    public abstract class Sensor : MonoBehaviour, ISensor
    {
        //[SerializeField] private ScriptableObject _senses;
        [SerializeField] private float _tickInterval;
        private CountdownTimer _timer;
        private MyWorld.AIAgent _agent;
        private AIContext _context;

        public AIContext Context => _context;
        public float TickInterval => _tickInterval;

        // TODO: remove this when ISensor has been updated

        //public float TickRate => _tickInterval;
        //public float NextTickTime { get; set; }

        protected void Start()
        {
            _agent = GetComponent<AIAgent>();
            Debug.Assert(_agent != null, "No AIAgent found on " + gameObject.name);
            _context = _agent.Context;
            _timer = new CountdownTimer(_tickInterval);
            _timer.OnTimerStop += () => OnTimerStop();
            _timer.Start();
        }
        
        void Update()
        {
            _timer.Tick(Time.deltaTime);
        }

        public abstract void Tick(AIContext _context);
        public abstract void DrawGizmos(AIContext context);

        void OnTimerStop()
        {
            Tick(_context);
            _timer.Start();
        }
    }
}