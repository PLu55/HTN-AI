
namespace MyWorld
{
    public interface ISensor
    {
        float TickInterval { get; }
        public AIContext Context { get; }
        void Tick(AIContext context);
        void DrawGizmos(AIContext context);
    }
}