
using System;
using FluidHTN;
using UnityEngine;

// Work in progress!
namespace MyWorld
{
    public class GameObjectSensor : Sensor, ISensor
    {
        
        [SerializeField][Tooltip("How often should we update our knowledge about the whereabouts of bridges.")]
        private float _tickRate = 1f;

        //public float TickRate => _tickRate;
        //public float NextTickTime { get; set; }

        private Type _type;
        private static Collider[] _hits = new Collider[128];
          
        public override void Tick(AIContext context)
        {
            context.KnownWorkshops.Clear();
            var hitCount = Physics.OverlapSphereNonAlloc(context.Position, context.Senses.EyeSight, _hits);
            if (hitCount > 0)
            {
                for (var i = 0; i < hitCount; i++)
                {
                    Debug.Log($"Found a GameObject: {_hits[i].name}");
                    var resource = _hits[i].GetComponent<Workshop>();
                    if (resource != null)
                    {
                        Debug.Log("Found a GameObject!");
                        context.KnownWorkshops.Add(resource);
                    }
                }
            }
            context.SetState(AIWorldState.HasResourceInSight, context.KnownResources.Count > 0, EffectType.Permanent);
        }
        public override void DrawGizmos(AIContext context)
        {
            foreach (var resource in context.KnownResources)
            {
                if (resource == context.CurrentResource)
                {
                    Gizmos.color = new Color(0f, 1f, 1f, 0.85f);
                }
                else
                {
                    Gizmos.color = new Color(0f, 1f, 1f, 0.15f);
                }
                Gizmos.DrawLine(context.Head.position, resource.transform.position + Vector3.up * 2f);
                Gizmos.DrawSphere(resource.transform.position + Vector3.up * 2f, 0.25f);
            }
        }
    }
}