using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.AI;
using FluidHTN;

namespace MyWorld
{
    public class AIAgent : MonoBehaviour
    {
        [SerializeField][Tooltip("The domain definition for this agent")]
        private AIDomainDefinition _domainDefinition;

        [SerializeField] [Tooltip("The sensing capabilities of our agent")]
        private AISenses _senses;
        [SerializeField] [Tooltip("The radius to search for lost enemies")]
        private float _searchRadius = 10f;
        [SerializeField] private float _searchInterval = 5f;

        [SerializeField] [Tooltip("A Transform representing the head of the agent")]
        private Transform _head;

        private Planner<AIContext> _planner;
        private Domain<AIContext> _domain;
        private AIContext _context;

        [SerializeField]
        private Bag _bag;

        public AIContext Context => _context;
        public float SearchRadius => _searchRadius;
        public float SearchDuration => _searchInterval;
        public Bag Bag => _bag;
        
        void Awake()
        {
            if (_domainDefinition == null)
            {
                Debug.LogError($"Missing domain definition in {name}!");
                gameObject.SetActive(false);
                return;
            }

            _head = transform;
            _planner = new Planner<AIContext>();
            _context = new AIContext(this, _senses, _head, GetComponent<Animator>(), GetComponent<NavMeshAgent>());
            _domain = _domainDefinition.Create();
            _context.KnownRecipes.Add(new Recipe("Axe", WorkshopType.Smithy,
                      new Dictionary<AIResourceType, float> { {AIResourceType.Wood, 0.5f}, {AIResourceType.IronOre, 0.5f}},
                      AIResourceType.Axe));

        }
        private void Update()
        {
            if (_planner == null || _domain == null || _context == null ) //|| _sensory == null)
            {
                return;
            }
            _context.Update();

            //_context.Animator.SetFloat("speed", _context.NavAgent.desiredVelocity.magnitude);

            if (_context.CanSense)
            {
                //_sensory.Tick(_context);
            }

            _planner.Tick(_domain, _context);

            if (_context.LogDecomposition)
            {
                UpdateDecompositionLog();
            }
        }
        private void UpdateDecompositionLog()
        {
            while (_context.DecompositionLog?.Count > 0)
            {
                var entry = _context.DecompositionLog.Dequeue();
                var depth = FluidHTN.Debug.Debug.DepthToString(entry.Depth);
                Console.ForegroundColor = entry.Color;
                Console.WriteLine($"{depth}{entry.Name}: {entry.Description}");
                Debug.Log($"{depth}{entry.Name}: {entry.Description}");
            }
            Console.ResetColor();
        }
        private void OnDrawGizmos()
        {
            if (_context == null)
                return;

            _senses?.DrawGizmos(_context);
            //_sensory?.DrawGizmos(_context);

    #if UNITY_EDITOR
            var task = _planner.GetCurrentTask();
            if (task != null)
            {
                Handles.Label(_context.Head.transform.position + Vector3.up, task.Name);
            }
    #endif
        }
    }
}