using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu
{
    public class TestingGuard : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        void Start()
        {
            
        }
        void Update()
        {
            float angle = Vector3.Angle(transform.forward, _target.position - transform.position);
            Debug.Log($"PlayerSensor: angle to target: {angle}");
            float cos = Vector3.Dot(transform.forward , (_target.position - transform.position).normalized);
            Debug.Log($"PlayerSensor: cos to target: {cos}");
        }
    }
}