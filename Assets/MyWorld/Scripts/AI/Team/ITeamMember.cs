using System;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.AI.Team
{
	public interface ITeamMember
    {
        TeamController TeamController { get; }
        void SetController(TeamController controller);

    }

}
