using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.AI.Team
{
    public class TeamController : MonoBehaviour
    {
        [SerializeField] private string _teamName;

        private List<TeamMember> _members;

        void Awake()
        {
            _members = new List<TeamMember>();
        }
        
        void Start()
        {
            
        }
        void Update()
        {
            
        }
        public void AddMember(TeamMember member)
        {
            _members.Add(member);
        }
    }
}
