using System;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.AI.Team
{
    [Serializable]
    public class TeamMember : MonoBehaviour, ITeamMember
    {
        [SerializeField] private TeamController _teamController;

        public TeamController TeamController => _teamController;

        public void SetController(TeamController controller)
        {
            _teamController = controller;
        }
        void Start()
        {
            _teamController.AddMember(this);
        }

        public void Move(Vector3 position)
        {
            ComSystem.ComController.Instance.PositionMessage("Move", position);
        }

    }
}
