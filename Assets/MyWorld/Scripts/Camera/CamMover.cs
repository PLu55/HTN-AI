using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu
{
    public class CamMover : MonoBehaviour
    {
        [SerializeField] private float _speed = 5f;
        [SerializeField] private float _turnSpeed = 100f;

        public float Speed => _speed;

        void Update()
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {

                float horizontalInput = Input.GetAxis("Horizontal");
                float verticalInput = Input.GetAxis("Vertical");
                
                float rotationAmount = horizontalInput * _turnSpeed * Time.deltaTime;
                transform.Rotate(Vector3.up, rotationAmount);

                Vector3 movement = transform.forward * verticalInput * _speed * Time.deltaTime;
                transform.Translate(movement);
            }
        }
    }
}
