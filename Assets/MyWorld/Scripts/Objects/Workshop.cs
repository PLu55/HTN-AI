using UnityEngine;

namespace MyWorld
{
    public class Workshop : MonoBehaviour
    {
        [SerializeField]
        public WorkshopType type  = WorkshopType.Generic;

        public WorkshopType WorkshopType
        {
            get => type;
        }
    }
}
