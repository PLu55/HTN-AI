using UnityEngine;

namespace MyWorld
{
    public class Resource : MonoBehaviour
    {
        [SerializeField]
        float initialAmount = 0.5f;
        [SerializeField]
        AIResourceType resourceType = AIResourceType.Generic;
        public AIResourceType ResourceType => resourceType;
        [SerializeField]
        public float Amount { get; set; }

        public float LastTimeVisited { get; set; }

        void Awake()
        {
            Amount = initialAmount;
        }
    }
}
