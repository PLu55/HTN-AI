namespace MyWorld
{
    public enum AIResourceType
    {
        Generic,
        Wood,
        IronOre,
        Axe,
        Count
    }
}