using System;
using System.Collections.Generic;
using UnityEngine;

namespace MyWorld
{
    public class Recipe : ScriptableObject
    {
        public string Name { get; }
        public WorkshopType WorkshopType { get; }
        public Dictionary<AIResourceType, float> resources = new Dictionary<AIResourceType, float>();
        public AIResourceType Artefact { get; }
        public Recipe(string name, WorkshopType workshopType,
                      Dictionary<AIResourceType, float> _resources,
                      AIResourceType artefact)
        {
            Name = name;
            resources = _resources;
            Artefact = artefact; 
            WorkshopType = workshopType;
        }
        public bool HasResources(Bag bag)
        {
            foreach (KeyValuePair<AIResourceType, float> kvp in resources)
            {
                if (bag.GetResource(kvp.Key) < kvp.Value)
                {
                    return false;
                }
            }
            return true;
        }
        public void CreateArtefact(Bag bag)
        {
            if (!HasResources(bag))
                throw new Exception("Recipe: Can't consume resources, not in bag.");
            foreach (KeyValuePair<AIResourceType, float> kvp in resources)
            {
                bag.AddResource(kvp.Key, -kvp.Value);
            }
            bag.AddResource(Artefact, 1);
        }
    }
}