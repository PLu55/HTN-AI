namespace MyWorld
{
    public class Human : Mobile
    {
        public override MobileType Type => MobileType.Human;
    }
}