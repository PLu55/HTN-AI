using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WorkshopType
{
    Generic,
    Smithy,
    Count
}
