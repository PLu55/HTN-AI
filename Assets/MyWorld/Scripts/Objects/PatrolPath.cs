using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace MyWorld
{
    public class PatrolPath : MonoBehaviour
    {
        [SerializeField] private float patrolSpeed = 1.0f;
        [SerializeField] private List<Transform> _patrolPath;

        public float PatrolSpeed => patrolSpeed;
        public List<Transform> PatrolPoints => _patrolPath;
    }
}
